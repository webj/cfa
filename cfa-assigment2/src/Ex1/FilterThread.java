package Ex1;

public class FilterThread extends Thread{
	
	private final int threadid;
	private Counter counter;
	private int accnr;
	private Filter filter;
	
	public FilterThread(int index, Filter filter, Counter counter){
		
		this.threadid = index;
		this.accnr = 0;
		this.filter = filter;
		this.counter = counter;
	}
	
	public void run(){
		
		while(true){
			filter.lock(threadid);
			if(this.counter.getCounter()<300000){
				this.counter.incrementCounter();
				accnr++;
				filter.unlock(threadid);	
			}
			
			else{
				filter.unlock(threadid);
				break;
			}
		}
		System.out.println("Thread Nr. "+threadid+" had "+ accnr + " increments.");
	}
	

	
	

}
