package src.Ex1;

public class Counter {
	
	private int counter;
	
	
	public Counter(){
		
		this.counter = 0;
		
	}
	
	public void incrementCounter(){
		counter++;
	}
	
	public int getCounter(){
		
		return counter;
	}

}
