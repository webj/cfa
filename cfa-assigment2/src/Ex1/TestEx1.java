package Ex1;

import java.util.ArrayList;

public class TestEx1 {
	
	
	public static void main(String[] args){
		
		ArrayList<FilterThread> threads = new ArrayList<FilterThread>();
		
		int numberOfThreads = 10;
		
		Counter counter = new Counter();
		Filter filter = new Filter(numberOfThreads);
		
		for(int i=0;i<numberOfThreads;i++){
			FilterThread t = new FilterThread(i, filter, counter);
			t.start();
			threads.add(t);			
		}
		
		for(Thread t : threads){
			while(t.isAlive()){
				
			}
		}
		
		System.out.println("Total increments: " + counter.getCounter());
		
		
		
	}

}
