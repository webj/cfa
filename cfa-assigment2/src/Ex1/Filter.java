package src.Ex1;

import java.util.concurrent.atomic.AtomicInteger;



public class Filter {

	private AtomicInteger level[];
	private AtomicInteger victim[];
	private final int n;
	private Counter counter;
	
	public Filter(int n){
		level = new AtomicInteger[n];
		victim = new AtomicInteger[n];
		this.n = n;
		this.counter = new Counter();
		
		for(int i=0;i<n;i++){
			level[i] = new AtomicInteger(0);
			victim[i] = new AtomicInteger(0);
		}
	}
	

	public void lock(int threadid) {
		// TODO Auto-generated method stub
		for(int L = 1; L<this.n;L++){
			
			level[threadid].set(L);
			victim[L].set(threadid);
			
			while(existThreadWithHigherLevel(L, threadid) && victim[L].get() == threadid){}			
		}
		
	}
	
	private boolean existThreadWithHigherLevel(int l, int threadid) {
		
		for(int i=0;i<this.n;i++){
			if (threadid!=i && level[i].get()>=l)
				return true;
		}		
		return false;
	}


	public void unlock(int threadid) {
		level[threadid]= new AtomicInteger(0);
		
	}
		
}
