package threads;

import java.util.concurrent.atomic.AtomicInteger;

import lists.Queue;

public class EnqThread extends Thread {

	private volatile Queue q;
	private int enqs;
	public static int numberOfThreads;
	public volatile AtomicInteger mySyncPoint;

	public EnqThread(Queue q, int enqs, int numberOfThreads,
			AtomicInteger mySyncPoint) {

		this.q = q;
		this.enqs = enqs;
		EnqThread.numberOfThreads = numberOfThreads;
		this.mySyncPoint = mySyncPoint;
	}

	public void run() {

		mySyncPoint.addAndGet(1);
		while (mySyncPoint.get() < numberOfThreads) {

		}
		;

		for (int i = 0; i < enqs; i++) {
			q.enq(i);
		}

	}

}
