package threads;

import java.util.concurrent.atomic.AtomicInteger;

import lists.Queue;

public class DeqThread extends Thread {

	private volatile Queue q;
	private int deqs;
	public static int numberOfThreads;
	public volatile AtomicInteger mySyncPoint;

	public DeqThread(Queue q, int deqs, int numberOfThreads,
			AtomicInteger mySyncPoint) {

		this.q = q;
		this.deqs = deqs;
		DeqThread.numberOfThreads = numberOfThreads;
		this.mySyncPoint = mySyncPoint;
	}

	public void run() {

		mySyncPoint.addAndGet(1);
		while (mySyncPoint.get() < numberOfThreads) {

		}
		;

		for (int i = 0; i < deqs; i++) {
			try {
				q.deq();
			} catch (Exception e) {

			}
		}

	}

}
