package tests;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

import lists.Queue;
import lists.UnboundedLockFreeQueue;
import lists.UnboundedLockQueue;
import threads.DeqThread;
import threads.EnqThread;

public class QueueTest {

	public static void main(String[] args) {

		System.out.println("Number of Threads");
		Scanner in = new Scanner(System.in);
		int numberOfThreads = in.nextInt();
		System.out.println("Number of Testruns:");
		int testruns = in.nextInt();
		System.out.println("Number of deqs and enqs:");
		int enqdeqs = in.nextInt();
		System.out.println("lock (1), lock free (2)");
		int which = in.nextInt();
		int operations = enqdeqs / numberOfThreads;
		ArrayList<Double> time = new ArrayList<Double>();
		ArrayList<Integer> emptyqueues = new ArrayList<Integer>();
		AtomicInteger mySyncPoint = new AtomicInteger(0);

		for (int j = 0; j < testruns; j++) {
			ArrayList<Thread> threads = new ArrayList<Thread>();

			Queue q = null;

			if (which == 1)
				q = new UnboundedLockQueue();
			else
				q = new UnboundedLockFreeQueue();

			Thread thread = new Thread();

			long start = System.nanoTime();

			for (int i = 0; i < numberOfThreads; i++) {
				if (i % 2 == 0) {
					thread = new EnqThread(q, operations, numberOfThreads,
							mySyncPoint);
				} else {
					thread = new DeqThread(q, operations, numberOfThreads,
							mySyncPoint);
				}
				thread.start();
				threads.add(thread);
			}

			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}

			long stop = System.nanoTime();

			time.add((stop - start) / Math.pow(10, 6));
			emptyqueues.add(q.getEmptyQueues());

		}

		printOutput(time, emptyqueues, testruns);
	}

	private static void printOutput(ArrayList<Double> time,
			ArrayList<Integer> emptyqueues, int testruns) {
		for (int i = 0; i < time.size(); i++) {
			System.out.println(time.get(i).toString());
			System.out.println("Empty queues: " + emptyqueues.get(i));

			if (i == testruns - 1) {
				System.out.println();
			}
		}
		double tot = 0;
		int totempt = 0;
		for (Double t : time) {
			tot = tot + t;

		}
		for (Integer i : emptyqueues) {
			totempt = totempt + i;
		}
		double avg = (tot / testruns);
		double avgempt = 0;
		if (totempt > 0)
			avgempt = (totempt / testruns);
		System.out.println("Average time: " + avg + " ms");
		System.out.println("Average empt queues: " + avgempt);

	}
}
