package lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class UnboundedLockFreeQueue implements Queue {

	private AtomicReference<AtomicNode> tail;
	private AtomicReference<AtomicNode> head;
	private AtomicInteger emptyQueue;

	public UnboundedLockFreeQueue() {
		AtomicNode start = new AtomicNode(0);
		head = new AtomicReference<AtomicNode>(start);
		tail = new AtomicReference<AtomicNode>(start);
		emptyQueue = new AtomicInteger(0);
	}

	public void enq(int key) {
		AtomicNode newNode = new AtomicNode(key);
		while (true) {

			AtomicNode last = this.tail.get();
			AtomicNode next = last.next.get();

			if (last.equals(tail.get())) {
				if (next == null) {
					if (last.next.compareAndSet(next, newNode)) {
						tail.compareAndSet(last, newNode);
						return;
					}
				} else {
					tail.compareAndSet(last, next);
				}
			}
		}

	}

	public int deq() throws Exception {
		while (true) {

			AtomicNode first = head.get();
			AtomicNode last = tail.get();

			AtomicNode next = first.next.get();
			if (first.equals(head.get())) {
				if (first.equals(last)) {
					if (next == null) {
						emptyQueue.getAndAdd(1);
						throw new Exception();
					}
					tail.compareAndSet(last, next);
				} else {
					int value = next.key;
					if (head.compareAndSet(first, next))
						return value;
				}
			}
		}
	}

	public int getEmptyQueues() {
		return emptyQueue.get();
	}
}
