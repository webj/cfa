package lists;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class UnboundedLockQueue implements Queue {

	private ReentrantLock enqLock;
	private ReentrantLock deqLock;
	protected final int END = 2147483647;
	protected final int START = -2147483647;
	protected Node tail;
	protected Node head;
	private volatile AtomicInteger emptyQueue;

	public UnboundedLockQueue() {
		this.enqLock = new ReentrantLock();
		this.deqLock = new ReentrantLock();
		this.emptyQueue = new AtomicInteger(0);
		createList();
	}

	private void createList() {
		this.head = new Node(START);
		this.tail = head;

	}

	public void enq(int key) {

		enqLock.lock();
		try {
			Node newNode = new Node(key);
			tail.next = newNode;
			tail = newNode;
		} finally {
			enqLock.unlock();
		}

	}

	public int deq() throws Exception {

		int result;
		deqLock.lock();
		try {
			if (this.head.next == null) {
				this.emptyQueue.addAndGet(1);
				throw new Exception();
			}
			result = this.head.next.key;
			this.head = this.head.next;
		} finally {
			deqLock.unlock();
		}
		return result;
	}

	@Override
	public int getEmptyQueues() {
		return this.emptyQueue.get();
	}
}
