package lists;

import java.util.concurrent.atomic.AtomicReference;

public class AtomicNode {

	public int key;
	public AtomicReference<AtomicNode> next;

	public AtomicNode(int i) {
		this.key = i;
		next = new AtomicReference<AtomicNode>(null);
	}

	public String toString() {
		return "Node: " + key;
	}

}
