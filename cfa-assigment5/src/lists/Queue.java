package lists;

public interface Queue {

	public void enq(int key);

	public int deq() throws Exception;

	public int getEmptyQueues();

}
