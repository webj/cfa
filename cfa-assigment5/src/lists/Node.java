package lists;

import java.util.concurrent.locks.ReentrantLock;

public class Node {

	public int key;
	public Node next;
	ReentrantLock lock;

	public Node(int i) {
		this.key = i;
		next = null;
		this.lock = new ReentrantLock();
	}

	public void lock() {
		this.lock.lock();
	}

	public void unlock() {
		this.lock.unlock();
	}

	public String toString() {
		return "Node: " + key;
	}

}
