package ex1tests;
import java.util.ArrayList;
import java.util.Scanner;

import ex1.CCASLock;
import ex1.CCASThread;
import ex1.Counter;



public class CCASLockTest {
	
	
	public static void main(String[] args){
		
		
		
		System.out.println("Number of Threads");
		Scanner in = new Scanner(System.in);
		int numberOfThreads = in.nextInt();
		
		
		for(int j=0;j<3;j++){
			ArrayList<CCASThread> threads = new ArrayList<CCASThread>();
			Counter counter = new Counter();
			CCASLock lock = new CCASLock();
		
			long start = System.currentTimeMillis();
			
			for(int i=0;i<numberOfThreads;i++){
				CCASThread t = new CCASThread(i, counter,lock);
				t.start();
				threads.add(t);			
			}
			
			for(Thread t : threads){
				try {
					t.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			long stop = System.currentTimeMillis();
			System.out.println("Total increments: " + counter.getCounter() + " Time: " + (stop-start));
		}
		
		
		
	}
}
