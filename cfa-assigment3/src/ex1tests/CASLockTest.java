package ex1tests;
import java.util.ArrayList;
import java.util.Scanner;

import ex1.CASLock;
import ex1.CASThread;
import ex1.Counter;




public class CASLockTest {
	
	
	public static void main(String[] args){
		
		System.out.println("Number of Threads");
		Scanner in = new Scanner(System.in);
		int numberOfThreads = in.nextInt();
		
		for(int j=0;j<3;j++){
		
			ArrayList<CASThread> threads = new ArrayList<CASThread>();
			Counter counter = new Counter();
			CASLock lock = new CASLock();
			
			long start = System.currentTimeMillis();
			
			for(int i=0;i<numberOfThreads;i++){
				CASThread t = new CASThread(i, counter,lock);
				t.start();
				threads.add(t);			
			}
			
			for(Thread t : threads){
				try {
					t.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			long stop = System.currentTimeMillis();
			
			System.out.println("Total increments: " + counter.getCounter() + " Time: " + (stop-start));
		}
		
		
		
	}
}
