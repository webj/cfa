package ex1;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


public class CCASLock implements Lock {
	
	private AtomicInteger tas;
	
	
	public CCASLock(){
		tas = new AtomicInteger(0);
	}
	
	@Override
	public void lock() {
		// TODO Auto-generated method stub
		while(true){
			while(tas.get()==1){}
			if(tas.compareAndSet(0, 1))
				return;
		}
	}
	@Override
	public void unlock() {
		// TODO Auto-generated method stub
		tas.set(0);

	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}


}
