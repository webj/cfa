package ex1;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


public class CASLock implements Lock {

	
	private AtomicInteger tas;
	
	public CASLock(){
		tas = new AtomicInteger(0);
	}
	
	@Override
	public void lock() {
		
		while(!tas.compareAndSet(0, 1)){}
		
		
		// TODO Auto-generated method stub

	}
	
	
	@Override
	public void unlock() {
		// TODO Auto-generated method stub
		tas.set(0);

	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long arg0, TimeUnit arg1)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}



}
