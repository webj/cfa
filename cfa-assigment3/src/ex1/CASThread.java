package ex1;
public class CASThread extends Thread{
	
	private final int threadid;
	private Counter counter;
	private int accnr;
	private CASLock lock;
	
	public CASThread(int index, Counter counter, CASLock lock){
		
		this.threadid = index;
		this.accnr = 0;
		this.counter = counter;
		this.lock = lock;
	}
	
	public void run(){
		
		while(true){
			lock.lock();
			if(this.counter.getCounter()<300000){
				this.counter.incrementCounter();
				accnr++;
				lock.unlock();	
			}
			
			else{
				lock.unlock();
				break;
			}
		}
		System.out.println("Thread Nr. "+threadid+" had "+ accnr + " increments.");
	}
	

	
	

}

