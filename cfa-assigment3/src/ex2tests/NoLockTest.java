package ex2tests;

import java.util.ArrayList;

import ex2.DecThread;
import ex2.EnqThread;
import ex2.LockFreeList;





public class NoLockTest {
	

	public static void main(String[] args){
		int numberOfThreads = 2;	
		
		ArrayList<Thread> threads = new ArrayList<Thread>();
		LockFreeList list = new LockFreeList(numberOfThreads);
	
		Thread thread = new Thread();
		
		long start = System.currentTimeMillis();
		
		for(int i=0;i<numberOfThreads;i++){
			if(i%2==0)
				thread = new DecThread(list);
			else{
				thread = new EnqThread(list);
			}
			thread.start();
			threads.add(thread);			
		}
		
		for(Thread t : threads){
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long stop = System.currentTimeMillis();
		
		System.out.println("OneLockList Time: " + (stop-start));
	}
}

