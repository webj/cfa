package ex2tests;

import java.util.ArrayList;
import java.util.Scanner;

import ex2.DecThread;
import ex2.EnqThread;
import ex2.TwoLockList;





public class TwoLockTest {		

		public static void main(String[] args){
			System.out.println("Number of Threads");
			Scanner in = new Scanner(System.in);
			int numberOfThreads = in.nextInt();	
			
			for(int j=0;j<3;j++){
			
				ArrayList<Thread> threads = new ArrayList<Thread>();
				TwoLockList list = new TwoLockList(numberOfThreads);
				Thread thread = new Thread();	
				
				long start = System.currentTimeMillis();
			
				for(int i=0;i<numberOfThreads;i++){
							
					if(i%2==0)
						thread = new DecThread(list);
					else{
						thread = new EnqThread(list);
					}
					thread.start();
					threads.add(thread);			
				}
				
				for(Thread t : threads){
					try {
						t.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				long stop = System.currentTimeMillis();				
				System.out.println("TwoLockList Time: " + (stop-start));
			}
		}
}




