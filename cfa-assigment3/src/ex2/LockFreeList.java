package ex2;


public class LockFreeList extends AbstractQueue{
	
	
	public LockFreeList(int qsize){
		
		super(qsize);
	}
	
	public void enq(int x){
		
		while (this.tail-this.head == this.qsize);
		this.items[this.tail%this.qsize] = x;
		this.tail++;
	}
	
	public int deq(){

		while (this.tail == this.head);		
		int item = this.items[this.head%this.qsize];
		this.head++;
		return item;
	}
	
}


