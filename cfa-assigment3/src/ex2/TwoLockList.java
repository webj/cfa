package ex2;

import ex1.CCASLock;


public class TwoLockList extends AbstractQueue{
	
	private volatile CCASLock enqlock;
	private volatile CCASLock deqlock;
	
	public TwoLockList(int qsize){
		
		super(qsize);
		this.enqlock = new CCASLock();
		this.deqlock = new CCASLock();
	}
	
	
	
	public void enq(int x){
		enqlock.lock();
		while(tail-head == qsize);		
		items[tail%qsize] = x;
		tail++;
		enqlock.unlock();		
	}
	
	public int deq(){
		this.deqlock.lock();
		while(tail == head);
		int item = items[head%qsize];
		head++;
		deqlock.unlock();
		return item;			
	}






}




