package ex2;

import ex1.CCASLock;


public class OneLockList extends AbstractQueue{
	
	private volatile CCASLock lock;
	
	public OneLockList(int qsize){
		
		super(qsize);
		lock = new CCASLock();
	}
	
	
	
	public void enq(int x){
		
		while (tail-head == qsize);
		lock.lock();
		items[tail%qsize] = x;
		tail++;
		lock.unlock();
	}
		
	
	
	public int deq(){		
		while (tail == head);	
		lock.lock();
		int item = items[head%qsize];
		head++;
		lock.unlock();
		return item;	
	}






}



