package ex2;

public abstract class AbstractQueue {
	
	protected volatile int  head, tail;
	protected volatile int items[];
	protected volatile int qsize;
	
	
	public AbstractQueue(int qsize){
		
		head = 0;
		tail = 0;
		items = new int[qsize];
		this.qsize = qsize;
	}
	
	public abstract int deq();	
	public abstract void enq(int x);

}
