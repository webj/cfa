package ex2;



public class DecThread extends Thread{
	
	AbstractQueue list;
	int counter;

	
	
	public DecThread(AbstractQueue list){
		this.list = list;
		counter = 0;
		
	}
	
	public void run(){
		while (counter<100000){			

			list.deq();
			counter++;

			
		}
		System.out.println("Thread " + this.getId() + " has " + counter + " dequeues");
	}

}
