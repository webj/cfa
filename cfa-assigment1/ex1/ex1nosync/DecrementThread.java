package ex1nosync;

public class DecrementThread extends Thread{
	
	private int local;
	private SharedObject object;
	
	public DecrementThread(SharedObject object){
		this.local = 0;
		this.object = object;
	}
	
	public void run(){
		int i=0;
		while(i<100000)	{
			decrement();
			i++;
		}
			
	}

	private void decrement() {
		
		this.local = object.getShared();
		object.setShared(--local);
		
	}
	
}
