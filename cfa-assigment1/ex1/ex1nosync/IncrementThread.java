package ex1nosync;

public class IncrementThread extends Thread{
	
	private int local;
	private SharedObject object;
	
	public IncrementThread(SharedObject object){
		this.local = 0;
		this.object = object;
	}
	
	public void run(){
		int i=0;
		while(i<100000)	{
			increment();
			i++;
		}
			
	}

	private void increment() {
		
		this.local = object.getShared();
		object.setShared(++local);
		
	}

}
