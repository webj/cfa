package ex1nosync;

public class SharedObject {
	
	private int shared;
	
	public SharedObject(){
		shared = 0;
	}
	
	public void setShared(int shared){
		this.shared = shared;
	}
	
	public int getShared(){
		
		return this.shared;
	}

}
