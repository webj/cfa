package ex1nosync;

import java.util.ArrayList;


public class TestSharedObject {
	

	public static void main(String [ ] args) 
	{
		int m = 8;
		int n = 8;
		
	
		
		
	
		for(int j = 0;j<1;j++){
			long start;
			long finish;
			
			start = System.nanoTime();
			
			ArrayList<Thread> threads = new ArrayList<Thread>();
			SharedObject object = new SharedObject();
			
			for(int k=0;k<n;k++){
				IncrementThread i = new IncrementThread(object);
				threads.add(i);
				i.start();
								
			}
			for(int i=0;i<m;i++){
				DecrementThread d = new DecrementThread(object);
				threads.add(d);
				d.start();
				
			}
			
			for(Thread t : threads){
				while(t.isAlive()){
					
				}
			}
			
			finish = System.nanoTime();
			
			System.out.println((finish-start)/1000000 + "ms");
			
			System.out.println("Value of SharedVariable: " + object.getShared()); 
		}
		
	}
	


}
