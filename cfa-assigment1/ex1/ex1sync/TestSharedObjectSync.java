package ex1sync;

import java.util.ArrayList;

public class TestSharedObjectSync {
	

	public static void main(String [ ] args) throws InterruptedException
	{
		{
			int m = 8;
			int n = 8;
			
		
		
			for(int j = 0;j<1;j++){
				
				long start;
				long finish;
				
				start = System.nanoTime();
						
				ArrayList<Thread> threads = new ArrayList<Thread>();
				SharedObjectSync object = new SharedObjectSync();
				
				for(int k=0;k<n;k++){
					IncrementThreadSync i = new IncrementThreadSync(object);
					i.start();
					threads.add(i);
					
									
				}
				for(int i=0;i<m;i++){
					DecrementThreadSync d = new DecrementThreadSync(object);
					d.start();
					threads.add(d);					
				}
				
				for(Thread t : threads){
					while(t.isAlive()){
	
					}
				}
				
				finish = System.nanoTime();
				
				System.out.println((finish-start)/1000000 + "ms");
				
				System.out.println("Value of SharedVariable: " + object.getShared()); 
			}
			
		}


		
	}
	


}
