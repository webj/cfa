package ex1sync;

public class DecrementThreadSync extends Thread{
	
	private int local;
	private SharedObjectSync object;
	
	public DecrementThreadSync(SharedObjectSync object){
		this.local = 0;
		this.object = object;
	}
	
	public void run(){
		int i=0;
		while(i<100000)	{
		
			
				decrement();
				i++;
			
		}
			
	}

	private void decrement() {
		
		synchronized(object){
			this.local = object.getShared();
			object.setShared(--local);
		}
	}
	
}
