package ex1sync;

public class IncrementThreadSync extends Thread{
	
	private int local;
	private SharedObjectSync object;
	
	public IncrementThreadSync(SharedObjectSync object){
		this.local = 0;
		this.object = object;
	}
	
	public void run(){
		int i=0;
		while(i<100000)	{
			
		
			increment();
			i++;
			
		}
			
	}

	public void increment() {
		
			synchronized(object){
				this.local = object.getShared();
				object.setShared(++local);
			}
	}

}
