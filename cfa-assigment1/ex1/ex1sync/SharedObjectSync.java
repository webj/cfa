package ex1sync;

public  class SharedObjectSync {
	
	private int shared;
	
	public SharedObjectSync(){
		shared = 0;
	}
	
	public void  setShared(int shared){
		 
			this.shared = shared;
		
	}
	
	public int getShared(){
		
		return this.shared;
	}

}
