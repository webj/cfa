package ex1reentrantlock;

public class SharedObjectReentrant {
	
	private int shared;
	
	public SharedObjectReentrant(){
		shared = 0;
	}
	
	public void setShared(int shared){
		this.shared = shared;
	}
	
	public int getShared(){
		
		return this.shared;
	}

}
