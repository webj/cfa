package ex1reentrantlock;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class TestSharedObjectReentrant {
	

	public static void main(String [ ] args) throws InterruptedException
	{
		int m = 8;
		int n = 8;
		

	
		for(int j = 0;j<1;j++){
			long start;
			long finish;
			
			start = System.nanoTime();
				ReentrantLock lock = new ReentrantLock();
				ArrayList<Thread> threads = new ArrayList<Thread>();
				SharedObjectReentrant object = new SharedObjectReentrant();
	
				
				for(int k=0;k<n;k++){
					IncrementThreadReentrant i = new IncrementThreadReentrant(object, lock);
					threads.add(i);
					i.start();
									
				}
				for(int i=0;i<m;i++){
					DecrementThreadReentrant d = new DecrementThreadReentrant(object, lock);
					threads.add(d);
					d.start();
					
				}
				
				for(Thread t : threads){
					while(t.isAlive()){
						
					}
				}
			finish = System.nanoTime();
			
			System.out.println((finish-start)/1000000 + "ms");
			System.out.println("Value of SharedVariable: " + object.getShared()); 
		}
		
	}
	


}
