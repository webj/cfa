package ex1reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

public class IncrementThreadReentrant extends Thread{
	
	private int local;
	private SharedObjectReentrant object;
	private ReentrantLock lock;
	
	public IncrementThreadReentrant(SharedObjectReentrant object, ReentrantLock lock){
		this.local = 0;
		this.object = object;
		this.lock = lock;
	}
	
	public void run(){
		int i=0;
		while(i<100000)	{
			increment();
			i++;
		}
			
	}

	private void increment() {
		lock.lock();
		this.local = object.getShared();
		object.setShared(++local);
		lock.unlock();
	}

}
