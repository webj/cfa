package ex2savages1;

import java.util.ArrayList;

public class TestDinningSavages {

	public static void main(String [ ] args) {
		
		int n = 100;
		int numberOfSavages = 1009;
		ArrayList<Thread> savages = new ArrayList<Thread>();
		
		Pot pot = new Pot(n);
		
		Cook cook = new Cook(pot, numberOfSavages);
		cook.start();
		
		for(int i=0;i<numberOfSavages;i++){
			Savage s = new Savage("Ruedi"+i, pot);
			s.start();
			savages.add(s);
		}
		
		for(Thread t : savages){
			while(t.isAlive()){
				
			}
		}
		
		System.out.println("everybody has eaten " + pot.numberOfPortionsLeft());
		
		
	}
	
}
