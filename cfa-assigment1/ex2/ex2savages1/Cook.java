package ex2savages1;


public class Cook extends Thread{
	
	private Pot pot;
	private int portcooked;
	private int numberOfSavages;
	
	
	public Cook(Pot pot, int numberOfSavages){
		this.pot = pot;
		this.numberOfSavages = numberOfSavages;
		
	}
	
	
	public void run(){
		
		while(!everyBodyHasEaten()){
			
			
				synchronized(pot){
					if(pot.isEmpty()){
						pot.refill();
						this.portcooked+=pot.getPotSize();
						System.out.println("pot is empty, I refill pot");
						System.out.println("numberOfSavages: " + this.numberOfSavages + " number of port coocked: "+ this.portcooked);
	
					}
				}
		}
		
		System.out.println("everybody has eaten");
		
	}


	private boolean everyBodyHasEaten() {
		return numberOfSavages<=this.portcooked;
	}

}
