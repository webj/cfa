package ex2savages1;

public class Savage extends Thread{
	
	
	private String name;
	private Pot pot;
	private boolean haseaten;
	
	public Savage(String name, Pot pot){
		this.name = name;
		this.haseaten = false;
		this.pot = pot;
	}
	
	
	public void run(){
		
			while(!this.haseaten){		
				
					synchronized(pot){	
						if(!pot.isEmpty()){
							this.pot.getPortion();
							this.haseaten=true;
							System.out.println(name + " has eaten");
						}
					}
			}
			
	}
	
	public String getSavageName(){
		return name;
	
	}
	


}
