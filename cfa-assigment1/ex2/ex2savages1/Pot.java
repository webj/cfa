package ex2savages1;

public class Pot {
	
	private final int SIZE;
	private int portions;
	
	public Pot(int size){
		
		this.SIZE = size;
		portions=0;
		
	}
	
	
	public void refill(){
		portions += this.SIZE;
	}
	
	public void getPortion(){
		portions--;
	}
	
	public boolean isEmpty(){
		return portions==0;
	}
	
	public int getPotSize(){
		return SIZE;
	}
	
	public int numberOfPortionsLeft(){
		return portions;
	}



}
