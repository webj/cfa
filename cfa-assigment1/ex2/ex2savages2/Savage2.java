package ex2savages2;

public class Savage2 extends Thread{
	
	
	private String name;
	private Pot2 pot;
	private int numberOfMeals;
	
	public Savage2(String name, Pot2 pot){
		this.name = name;
		this.pot = pot;
		this.numberOfMeals=0;
	}
	
	
	public void run(){
		
			while(true){			
			
				
				synchronized(pot){	
					if(!pot.isEmpty() && this.numberOfMeals<=pot.getNrOfRounds()){
						this.pot.getPortion();
						this.numberOfMeals++;
						System.out.println(name + " has eaten");
					}
				}

			}
			//System.out.println("Savage " + name + " has eaten " + this.numberOfMeals);
			
	}

	


}
