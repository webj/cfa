package ex2savages2;

import java.util.ArrayList;

public class TestDinningSavages2 {

	public static void main(String [ ] args) {
		
		int n = 3;
		int numberOfSavages = 10;
		ArrayList<Thread> savages = new ArrayList<Thread>();
		
		Pot2 pot = new Pot2(n, numberOfSavages);
		
		Cook2 cook = new Cook2(pot);
		cook.start();
		
		for(int i=0;i<numberOfSavages;i++){
			Savage2 s = new Savage2("Ruedi"+i, pot);
			s.start();
			savages.add(s);
		}
		
		for(Thread t : savages){
			while(t.isAlive()){
				
			}
		}
		
		System.out.println("everybody has eaten " + pot.numberOfPortionsLeft());
		
		
	}
	
}
