package ex2savages2;

public class Pot2 {
	
	private final int SIZE;
	private int portions;
	private int nrOfSavagesEaten;
	private final int NUMBER_OF_SAVAGES;
	private int nrOfRounds;
	
	public Pot2(int size, int numberOfSavages){
		
		this.SIZE = size;
		portions=0;
		this.NUMBER_OF_SAVAGES = numberOfSavages;
		nrOfRounds = 0;
		nrOfSavagesEaten=0;
	}
	
	
	public void refill(){
		portions += this.SIZE;
		System.out.println(portions);
	}
	
	public void getPortion(){
		portions--;
		nrOfSavagesEaten++;
		allSavagesHaveEaten();
	}
	
	public boolean isEmpty(){
		return portions==0;
	}
	
	public int getPotSize(){
		return SIZE;
	}
	
	public int numberOfPortionsLeft(){
		return portions;
	}
		
	public boolean allSavagesHaveEaten(){
		
		if(nrOfSavagesEaten == NUMBER_OF_SAVAGES){
			System.out.println(this.nrOfSavagesEaten);
			System.out.println(this.nrOfRounds);
			this.nrOfRounds++;
			this.nrOfSavagesEaten=0;
			return true;
		}
		else{
			//System.out.println(this.nrOfSavagesEaten);
			return false;
		}
	}
	
	public int getNrOfRounds(){
		return this.nrOfRounds;
	}



}
