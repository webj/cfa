package ex1;

public class RemoveNodeThread extends Thread {

	private int[] randoms;
	private volatile LockList list;
	private boolean debug;

	public RemoveNodeThread(int[] randoms, LockList list, boolean debug) {
		this.randoms = randoms;
		this.list = list;
		this.debug = debug;
	}

	public void run() {

		for (int i = 0; i < randoms.length; i++) {

			if (debug) {

				if (list.remove(randoms[i])) {
					System.out.println(this.getId() + " removing: "
							+ randoms[i]);
				} else {
					System.out.println(this.getId() + " could remove node "
							+ randoms[i]);
				}
			} else
				list.remove(randoms[i]);
		}

	}

}
