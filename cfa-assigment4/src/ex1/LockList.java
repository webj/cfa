package ex1;

public abstract class LockList {

	protected final int END = 2147483647;
	protected final int START = -2147483647;
	protected Node tail;
	protected Node head;

	public LockList() {
		createList();
	}

	public abstract boolean add(int key);

	public abstract boolean remove(int key);

	private void createList() {
		this.head = new Node(START);
		head.next = new Node(END);
		this.tail = head.next;

	}

	public String toString() {
		Node n = this.head;
		StringBuffer sb = new StringBuffer();

		sb.append(n.key);
		n = n.next;

		while (n != null && n.key <= tail.key) {

			sb.append(", ");
			sb.append(n.key);
			n = n.next;
		}

		return sb.toString();
	}

	public void print(String msg) {
		System.out.println(msg);
	}

	public int getLength() {

		Node n = this.head;
		int length = 0;
		while (n.key != tail.key) {
			length++;
			n = n.next;
		}

		return length - 1;
	}
}
