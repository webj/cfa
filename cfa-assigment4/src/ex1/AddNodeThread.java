package ex1;

public class AddNodeThread extends Thread {

	private int[] randoms;
	private volatile LockList list;
	private boolean debug;

	public AddNodeThread(int[] randoms, LockList list, boolean debug) {
		this.randoms = randoms;
		this.list = list;
		this.debug = debug;
	}

	public void run() {

		for (int i = 0; i < randoms.length; i++) {
			if (debug) {
				if (list.add(randoms[i])) {
					System.out.println(this.getId() + " adding: " + randoms[i]);
				} else {
					System.out.println(this.getId() + " could not add node "
							+ randoms[i]);
				}
			} else
				list.add(randoms[i]);
		}

	}

}
