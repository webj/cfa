package ex1;

public class OptimisticList extends LockList {

	public OptimisticList() {

		super();

	}

	@Override
	public boolean add(int key) {
		Node n = new Node(key);

		while (true) {
			Node pred = this.head;
			Node curr = pred.next;
			while (curr.key <= key) {
				if (key == curr.key) {
					// print("Key " + key + " is already in the list");
					break;
				}
				pred = curr;
				curr = curr.next;
			}
			try {
				pred.lock();
				curr.lock();
				if (key == curr.key) {
					// print("Key " + key + " is already in the list");
					return false;
				}

				if (validate(pred, curr)) {

					n.next = curr;
					pred.next = n;
					return true;
				}
			} finally {
				pred.unlock();
				curr.unlock();
			}
		}
	}

	@Override
	public boolean remove(int key) {

		while (true) {
			Node pred = this.head;
			Node curr = pred.next;
			while (curr.key <= key) {
				if (key == curr.key)
					break;
				pred = curr;
				curr = curr.next;
			}
			try {
				pred.lock();
				curr.lock();
				if (validate(pred, curr)) {
					if (curr.key == key) {
						pred.next = curr.next;
						return true;
					} else
						return false;
				}
			} finally {
				pred.unlock();
				curr.unlock();
			}
		}
	}

	private boolean validate(Node pred, Node curr) {

		Node node = this.head;
		while (node.key <= pred.key) {
			if (node == pred)
				return pred.next.equals(curr);
			node = node.next;
		}

		return false;
	}

}
