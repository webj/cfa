package ex1;

public class FineLockList extends LockList {

	public FineLockList() {

		super();

	}

	public boolean remove(int key) {

		Node pred = null, curr = null;

		try {
			pred = this.head;
			pred.lock();
			curr = pred.next;
			curr.lock();

			while (curr.key <= key) {
				if (key == curr.key) {

					pred.next = curr.next;
					return true;
				}
				pred.unlock();
				pred = curr;
				curr = curr.next;
				curr.lock();
			}
			return false;

		} finally {
			curr.unlock();
			pred.unlock();
		}
	}

	public boolean add(int key) {
		Node n = new Node(key);
		Node pred = null;
		Node curr = null;

		try {
			pred = this.head;
			pred.lock();
			curr = pred.next;
			curr.lock();

			while (curr.key <= key) {
				if (key == curr.key) {

					return false;
				}
				pred.unlock();
				pred = curr;
				curr = curr.next;
				curr.lock();
			}
			// add new node

			n.next = curr;
			pred.next = n;
			return true;

		} finally {

			curr.unlock();
			pred.unlock();
		}

	}

}
