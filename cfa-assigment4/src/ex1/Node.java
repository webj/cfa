package ex1;

import java.util.concurrent.locks.ReentrantLock;

public class Node {

	public int key;
	public Node next;
	ReentrantLock lock;

	public Node(int i) {
		this.key = i;
		this.lock = new ReentrantLock();
	}

	public void lock() {
		this.lock.lock();
	}

	public void unlock() {
		this.lock.unlock();
	}

	public String toString() {
		return "Node: " + key;
	}

}
