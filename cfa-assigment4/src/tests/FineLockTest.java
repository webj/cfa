package tests;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import ex1.AddNodeThread;
import ex1.FineLockList;
import ex1.LockList;
import ex1.RemoveNodeThread;

public class FineLockTest {

	public static int numberOfRand = 100000;
	private static boolean debug = false;

	public static void main(String[] args) {

		System.out.println("Number of Threads");
		Scanner in = new Scanner(System.in);
		int numberOfThreads = in.nextInt();
		System.out.println("Number of Testruns:");
		int testruns = in.nextInt();
		ArrayList<Long> time = new ArrayList<Long>();
		ArrayList<Integer> length = new ArrayList<Integer>();

		ArrayList<int[]> trandoms = generateRandoms(numberOfThreads);

		for (int j = 0; j < testruns; j++) {
			ArrayList<Thread> threads = new ArrayList<Thread>();
			LockList list = new FineLockList();

			Thread thread = new Thread();

			long start = System.currentTimeMillis();

			for (int i = 0; i < numberOfThreads; i++) {
				if (i % 2 == 0) {
					thread = new AddNodeThread(trandoms.get(i), list, debug);
				} else {
					thread = new RemoveNodeThread(trandoms.get(i), list, debug);
				}
				thread.start();
				threads.add(thread);
			}

			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// for (Thread t : threads) {
			// while (t.isAlive()) {
			//
			// }
			// }

			long stop = System.currentTimeMillis();

			// System.out.println("Runtime: " + (stop - start));
			// System.out.println(list.toString());
			// System.out.println("Length: " + list.getLength());
			// System.out.println();

			time.add(stop - start);
			length.add(list.getLength());

		}

		printOutput(time, length, testruns);
	}

	private static void printOutput(ArrayList<Long> time,
			ArrayList<Integer> length, int testruns) {
		for (int i = 0; i < time.size(); i++) {
			System.out.println(time.get(i).toString());

			if (i == testruns - 1) {
				System.out.println();
			}
		}

		for (int i = 0; i < time.size(); i++) {
			System.out.println(length.get(i).toString());

			if (i == testruns - 1) {
				System.out.println();
			}
		}

	}

	private static ArrayList<int[]> generateRandoms(int numberOfThreads) {
		ArrayList<int[]> trandoms = new ArrayList<int[]>();
		Random rand = new Random();

		for (int j = 0; j < numberOfThreads; j++) {
			trandoms.add(new int[(numberOfRand / numberOfThreads)]);
			for (int i = 0; i < trandoms.get(j).length; i++) {
				trandoms.get(j)[i] = rand.nextInt(100);

			}
		}

		return trandoms;
	}
}
