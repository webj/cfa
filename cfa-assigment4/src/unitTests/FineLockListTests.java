package unitTests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ex1.AddNodeThread;
import ex1.FineLockList;
import ex1.LockList;
import ex1.OptimisticList;
import ex1.RemoveNodeThread;

public class FineLockListTests {

	public LockList list;

	public void initializeFineLock() {
		this.list = new FineLockList();
	}

	public void initializeOptimisticLock() {
		this.list = new OptimisticList();
	}

	@Test
	public void fineLockTest() throws InterruptedException {

		int[] add = { 1, 2, 3, 4, 5 };
		int[] remove = { 3 };
		int[] remove2 = { 4 };
		boolean debug = false;
		initializeFineLock();

		Thread addT = new AddNodeThread(add, list, debug);
		Thread remT = new RemoveNodeThread(remove, list, debug);
		Thread remT2 = new RemoveNodeThread(remove2, list, debug);
		addT.start();
		addT.join();

		remT.start();
		remT2.start();
		remT.join();
		remT2.join();

		System.out.println(list.toString());
		assertEquals("-2147483647, 1, 2, 5, 2147483647", list.toString());

	}

	@Test
	public void optimisticListTest() throws InterruptedException {

		int[] add = { 1, 2, 3, 4, 5 };
		int[] remove = { 3 };
		int[] remove2 = { 4 };
		boolean debug = false;
		initializeOptimisticLock();

		Thread addT = new AddNodeThread(add, list, debug);
		Thread remT = new RemoveNodeThread(remove, list, debug);
		Thread remT2 = new RemoveNodeThread(remove2, list, debug);
		addT.start();
		addT.join();

		remT.start();
		remT2.start();
		remT.join();
		remT2.join();

		System.out.println(list.toString());
		assertEquals("-2147483647, 1, 2, 5, 2147483647", list.toString());

	}

	@Test
	public void addoptimisticListTest() throws InterruptedException {

		int[] add = { 1, 2, 3, 4, 5 };
		initializeOptimisticLock();
		boolean debug = false;

		Thread addT = new AddNodeThread(add, list, debug);

		addT.start();
		addT.join();
		System.out.println(list.toString());
		assertEquals("-2147483647, 1, 2, 3, 4, 5, 2147483647", list.toString());

	}
}
